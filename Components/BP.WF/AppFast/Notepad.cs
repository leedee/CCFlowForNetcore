﻿using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.Web;
using BP.En;
using BP.Port;
using BP.Sys;
using BP.CCBill.Template;

namespace BP.AppFast
{
    /// <summary>
    /// 记事本 属性
    /// </summary>
    public class NotepadAttr : EntityMyPKAttr
    {
        /// <summary>
        /// 名称
        /// </summary>
        public const string Name = "Name";
        /// <summary>
        /// 功能ID
        /// </summary>
        public const string DTStart = "DTStart";
        /// <summary>
        /// 功能来源
        /// </summary>
        public const string DTEnd = "DTEnd";
        /// <summary>
        /// 重复方式
        /// </summary>
        public const string Repeat = "Repeat";
        /// <summary>
        /// 位置
        /// </summary>
        public const string Local = "Local";
        /// <summary>
        /// 描述
        /// </summary>
        public const string MiaoShu = "MiaoShu";
        /// <summary>
        /// 提醒时间
        /// </summary>
        public const string DTAlert = "DTAlert";
        /// <summary>
        /// 组织编号
        /// </summary>
        public const string OrgNo = "OrgNo";
        /// <summary>
        /// 记录人
        /// </summary>
        public const string Rec = "Rec";
        /// <summary>
        /// 记录日期
        /// </summary>
        public const string RDT = "RDT";
    }
    /// <summary>
    /// 记事本
    /// </summary>
    public class Notepad : EntityMyPK
    {
        #region 基本属性
        /// <summary>
        /// 组织编号
        /// </summary>
        public string OrgNo
        {
            get { return this.GetValStrByKey(NotepadAttr.OrgNo); }
            set { this.SetValByKey(NotepadAttr.OrgNo, value); }
        }
        public string Rec
        {
            get { return this.GetValStrByKey(NotepadAttr.Rec); }
            set { this.SetValByKey(NotepadAttr.Rec, value); }
        }
        public string RDT
        {
            get { return this.GetValStrByKey(NotepadAttr.RDT); }
            set { this.SetValByKey(NotepadAttr.RDT, value); }
        }
        #endregion

        #region 构造方法
        /// <summary>
        /// 权限控制
        /// </summary>
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                if (WebUser.IsAdmin)
                {
                    uac.IsUpdate = true;
                    return uac;
                }
                return base.HisUAC;
            }
        }
        /// <summary>
        /// 记事本
        /// </summary>
        public Notepad()
        {
        }
        public Notepad(string mypk)
        {
            this.MyPK = mypk;
            this.Retrieve();
        }
        /// <summary>
        /// 重写基类方法
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_Notepad", "记事本");

                map.AddMyPK();
                map.AddTBString(NotepadAttr.Name, null, "标题", true, false, 0, 300, 10, true);

                map.AddTBDateTime(NotepadAttr.DTStart, null, "开始时间", true, false);
                map.AddTBDateTime(NotepadAttr.DTEnd, null, "结束时间", true, false);

                map.AddTBDateTime(NotepadAttr.DTAlert, null, "提醒时间", true, false);

                map.AddDDLSysEnum(NotepadAttr.Repeat, 0, "重复", true, false, "Repeat",
              "@0=永不@1=每年@2=每月");

                map.AddTBString(NotepadAttr.Local, null, "位置", true, false, 0, 300, 10, true);
                map.AddTBString(NotepadAttr.MiaoShu, null, "描述", true, false, 0, 300, 10, true);


                map.AddTBString(NotepadAttr.OrgNo, null, "OrgNo", false, false, 0, 100, 10);
                map.AddTBString(NotepadAttr.Rec, null, "记录人", false, false, 0, 100, 10, true);
                map.AddTBDateTime(NotepadAttr.RDT, null, "记录时间", false, false);


                //RefMethod rm = new RefMethod();
                //rm.Title = "方法参数"; // "设计表单";
                //rm.ClassMethodName = this.ToString() + ".DoParas";
                //rm.Visable = true;
                //rm.RefMethodType = RefMethodType.RightFrameOpen;
                //rm.Target = "_blank";
                ////rm.GroupName = "开发接口";
                ////  map.AddRefMethod(rm);

                //rm = new RefMethod();
                //rm.Title = "方法内容"; // "设计表单";
                //rm.ClassMethodName = this.ToString() + ".DoDocs";
                //rm.Visable = true;
                //rm.RefMethodType = RefMethodType.RightFrameOpen;
                //rm.Target = "_blank";
                ////rm.GroupName = "开发接口";
                //map.AddRefMethod(rm);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        #region 执行方法.
        protected override bool beforeInsert()
        {
            this.MyPK = DBAccess.GenerGUID();
            this.Rec = WebUser.No;
            this.RDT = DataType.CurrentDataTime;

            return base.beforeInsert();
        }
        #endregion 执行方法.
    }
    /// <summary>
    /// 记事本 s
    /// </summary>
    public class Notepads : EntitiesMyPK
    {
        /// <summary>
        /// 记事本
        /// </summary>
        public Notepads() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Notepad();
            }
        }
        #region 为了适应自动翻译成java的需要,把实体转换成List.
        /// <summary>
        /// 转化成 java list,C#不能调用.
        /// </summary>
        /// <returns>List</returns>
        public System.Collections.Generic.IList<Notepad> ToJavaList()
        {
            return (System.Collections.Generic.IList<Notepad>)this;
        }
        /// <summary>
        /// 转化成list
        /// </summary>
        /// <returns>List</returns>
        public System.Collections.Generic.List<Notepad> Tolist()
        {
            System.Collections.Generic.List<Notepad> list = new System.Collections.Generic.List<Notepad>();
            for (int i = 0; i < this.Count; i++)
            {
                list.Add((Notepad)this[i]);
            }
            return list;
        }
        #endregion 为了适应自动翻译成java的需要,把实体转换成List.
    }
}
